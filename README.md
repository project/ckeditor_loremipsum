## CKEditor Loremipsum

This module enables the Lorem ipsum plugin from CKEditor.com in your WYSIWYG.
This plugin allows to generate lorem ipsum sentence or paragraph easily, to
use in you web content, for example, it can be very useful when you want to
demonstrate a website or a portal.

### Requirements

CKEditor Module (Core)

CKEditor Lorem ipsum plugin: https://ckeditor.com/addon/loremipsum.

### Install

Install module via composer:

```bash
composer require drupal/ckeditor_loremipsum
drush en ckeditor_loremipsum
```

1. Download the plugin from https://ckeditor.com/addon/loremipsum.
2. Place the plugin in the root libraries' folder (/libraries).
3. Enable CKEditor Lorem ipsum in the Drupal admin.
4. Configure your WYSIWYG toolbar to include the button.

### Maintainers

George Anderson (geoanders)
https://www.drupal.org/u/geoanders
